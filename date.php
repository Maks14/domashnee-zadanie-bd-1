// Соединение c Хостингом Нетология

$host = 'localhost';
$db   = 'global';
$user = 'mvolkov';
$pass = 'neto1831';
$charset = 'utf8';

$dsn = "mysql:host=$host;dbname=$db;charset=$charset";

$pdo = new PDO($dsn, $user, $pass);


//Вывод данных

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
<style>
    table { 
        border-spacing: 0;
        border-collapse: collapse;
    }

    table td, table th {
        border: 1px solid #ccc;
        padding: 5px;
    }
    
    table th {
        background: #eee;
    }
</style>
</head>
<body>
<?php $stmt = $pdo->query('SELECT * FROM books'); ?>
<table>
  <tr>
    <th>Название</th>
    <th>Автор</th>
    <th>Год выпуска</th>
    <th>Жанр</th>
    <th>ISBN</th>
  </tr>
<?php while ($row = $stmt->fetch()) { ?>
  <tr>
    <td><?php echo $row['name']?></td>
    <td><?php echo $row['author']?></td>
    <td><?php echo $row['year']?></td>
    <td><?php echo $row['genre']?></td>
    <td><?php echo $row['isbn']?></td>
  <?php } ?> 
  </tr>
</table>
</body>
</html>